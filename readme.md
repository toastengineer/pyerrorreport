Send error reports and other messages to a central server you control.

Create a `Reporter` object with your server address and the TCP port the
server is running on (1234 by default) and use it's `.send()` method
to send a dict, made from `.send()`'s keyword args, to the server.

Launch the server with

    $ python -m error_report --hostname HOSTNAME --port PORT --keys KEYS

Default port is 1234.

Server reads a configuration JSON file at 

    ~/.pyerrorreport/config.json

Populate the json file with `hostname`, `port`, and `keys`.

`keys` is a list of the keys of values the server will read out of the
report dict to place the report in a directory.

I.e. if `group_by_keys` is `['userid', 'operation']` and the client sends
`{userid : "blabla", operation : "foobar", xyzzy : "plugh"}`
then the report will be saved in 

    [working directory]/reports/blabla/foobar/...
